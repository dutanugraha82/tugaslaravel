<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

use Illuminate\Support\Facades\Route;

Route::get('/', 'HomeController@Home');
Route::get('/register', 'AuthController@regist');
Route::get('/welcome', 'AuthController@welcome');
Route::post('/post','AuthController@welcome');
Route::get('/table', function(){
    return view('master');
});
Route::get('/data-tables' , function(){
    return view('data-tables');
});

